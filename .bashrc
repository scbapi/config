# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi

# User specific aliases and functions

if [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
fi

if ! pgrep -u $USER ssh-agent > /dev/null; then
    ssh-agent > ~/.ssh-agent-thing
fi
if [[ "$SSH_AGENT_PID" == "" ]]; then
    eval $(<~/.ssh-agent-thing) &>/dev/null
fi

ssh-add -l >/dev/null || alias ssh='ssh-add -l >/dev/null || ssh-add && unalias ssh; ssh'

# shell
[ -z "$PS1" ] && return
#PS1='\[\e[0;32m\]\u\[\e[m\] \[\e[1;34m\]\w\[\e[m\] \[\e[m\] \[\e[1;32m\]\$ \[\e[m\]\[\e[1;37m\] '

if [[ ${EUID} == 0 ]] ; then
PS1='\[\e[01;31m\]\h\[\e[01;34m\] \W \$\[\e[00m\] '
else
PS1='\[\e[01;32m\]\u@\h\[\e[01;34m\] \w \$\[\e[00m\] '
fi

complete -C '/usr/bin/aws_completer' aws

export EDITOR=vim
#set -o noclobber
shopt -sq checkwinsize
shopt -s histappend
export HISTSIZE=100000
export HISTFILESIZE=1000000
export HISTTIMEFORMAT='%F %T '
HISTCONTROL=ignoreboth
export PROMPT_COMMAND="history -a;history -c;history -r;$PROMPT_COMMAND"

# aliases
alias lo="ls -lha --group-directories-first"
alias ccat='pygmentize -g'
#alias ccat='pygmentize -g -O style=colorful,linenos=1'

alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias ll="ls -lh"
alias la="ls -a"
alias mv='mv -i'
alias rm='rm -i'
alias cp='cp -i'
alias exit="clear ; exit"

# aws completion
complete -C '/usr/local/aws/bin/aws_completer' aws

# function to decompress most of archives
ext () {
 if [ -f $1 ] ; then
     case $1 in
         *.tar.bz2)   tar xvjf $1    ;;
         *.tar.gz)    tar xvzf $1    ;;
         *.bz2)       bunzip2 $1     ;;
         *.rar)       rar x $1       ;;
         *.gz)        gunzip $1      ;;
         *.tar)       tar xvf $1     ;;
         *.tbz2)      tar xvjf $1    ;;
         *.tgz)       tar xvzf $1    ;;
         *.zip)       unzip $1       ;;
         *.Z)         uncompress $1  ;;
         *.7z)        7z x $1        ;;
         *)           echo "don't know how to extract '$1'..." ;;
     esac
 else
     echo "'$1' is not a valid file!"
 fi
}

# bash auto completion
export INPUTRC=~/.inputrc

# color less
export LESS='-R'
export LESSOPEN='|pygmentize -g %s'

# color man pages
man() {
    LESS_TERMCAP_md=$'\e[01;31m' \
    LESS_TERMCAP_me=$'\e[0m' \
    LESS_TERMCAP_se=$'\e[0m' \
    LESS_TERMCAP_so=$'\e[01;44;33m' \
    LESS_TERMCAP_ue=$'\e[0m' \
    LESS_TERMCAP_us=$'\e[01;32m' \
    command man "$@"
}

# ansible configurations
export ANSIBLE_CONFIG=~/.ansible/ansible.cfg
export ANSIBLE_HOSTS=~/.ansible/ec2.py
#export ANSIBLE_INVENTORY=~/.ansible/ec2.py
export EC2_INI_PATH=~/.ansible/ec2.ini

#export ANSIBLE_INVENTORY=~/.ansible/ansible_hosts

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

rmd () {
  pandoc $1 | lynx -stdin
}

# Welcome shell
echo -ne "Hello, $USER! Today is $(date).\n\n"
cal
echo -ne "System Info: $(uptime)\n\n"
